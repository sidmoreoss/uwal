package downloader

import (
	"fmt"
	"io"
	"net/http"
	"os"
	"path/filepath"
	"sync"

	"github.com/sidmoreoss/uwal/internal/utils"
	"github.com/vbauerster/mpb/v8"
	"github.com/vbauerster/mpb/v8/decor"
)

func downloadFile(url, destination string) error {
	resp, err := http.Get(url)
	if err != nil {
		return fmt.Errorf("error fetching %s: %w", url, err)
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("error fetching %s: status %s", url, resp.Status)
	}

	file, err := os.Create(destination)
	if err != nil {
		return fmt.Errorf("error creating file %s: %w", destination, err)
	}
	defer file.Close()

	_, err = io.Copy(file, resp.Body)
	if err != nil {
		return fmt.Errorf("error writing to file %s: %w", destination, err)
	}

	return nil
}

func downloadWallpaper(
	wg *sync.WaitGroup,
	bar *mpb.Bar,
	url, category, categoryDir string,
	errors *[]string,
	lock *sync.Mutex,
) {
	defer wg.Done()

	hashValue := utils.GetHash(url)
	fileName := fmt.Sprintf("%s_%s.jpg", category, hashValue)
	filePath := filepath.Join(categoryDir, fileName)

	if _, err := os.Stat(filePath); err == nil {
		bar.Increment()
		return
	}

	if err := downloadFile(url, filePath); err != nil {
		lock.Lock()
		*errors = append(*errors, fmt.Sprintf("Failed: %s (%v)", url, err))
		lock.Unlock()
	}
	bar.Increment()
}

func DownloadWallpapers(
	wallpaperData map[string][]string,
	installDir string,
	forceUpdate, noSubdirs bool,
	selectedCategory string,
) {
	// Handle force update
	if forceUpdate {
		utils.ShowMessage("Force update enabled. Deleting existing wallpapers...", utils.Yellow)

		if err := os.RemoveAll(installDir); err != nil {
			utils.ShowMessage(fmt.Sprintf("Error removing directory: %v", err), utils.Red)
			return
		}
	}

	// Create main directory
	err := os.MkdirAll(installDir, os.ModePerm)
	if err != nil {
		utils.ShowMessage(fmt.Sprintf("Error creating directory: %v", err), utils.Red)
		return
	}

	var wg sync.WaitGroup
	var errors []string
	var lock sync.Mutex

	for category := range wallpaperData {
		if selectedCategory != "" && category != selectedCategory {
			delete(wallpaperData, category)
		}
	}

	// Count total tasks
	totalTasks := 0
	for _, urls := range wallpaperData {
		totalTasks += len(urls)
	}

	// Initialize progress bar container
	progress := mpb.New(mpb.WithWidth(60))

	// Add a progress bar
	bar := progress.AddBar(
		int64(totalTasks),
		mpb.PrependDecorators(
			decor.Name("Downloading: ", decor.WCSyncSpaceR),
			decor.CountersNoUnit("%d/%d", decor.WCSyncSpaceR),
		),
		mpb.AppendDecorators(decor.Percentage(decor.WC{W: 5})),
	)

	for category, urls := range wallpaperData {
		categoryDir := installDir
		if !noSubdirs {
			categoryDir = filepath.Join(installDir, category)
		}

		err := os.MkdirAll(categoryDir, os.ModePerm)
		if err != nil {
			utils.ShowMessage(fmt.Sprintf("Error creating category directory: %v", err), utils.Red)
			continue
		}

		maxGoroutines := 12
		guard := make(chan struct{}, maxGoroutines)

		for _, url := range urls {
			wg.Add(1)
			guard <- struct{}{} // would block if guard channel is already filled
			go func(url string) {
				downloadWallpaper(&wg, bar, url, category, categoryDir, &errors, &lock)
				<-guard
			}(url)
		}
	}

	// Wait for all downloads and progress bar to finish
	wg.Wait()
	progress.Wait()

	// Print all accumulated errors at the end
	if len(errors) > 0 {
		utils.ShowMessage("\nSome downloads failed. Details:", utils.Red)
		for _, errMsg := range errors {
			fmt.Println(errMsg)
			fmt.Println()
		}
	} else {
		utils.ShowMessage("\nAll wallpapers downloaded successfully!", utils.Green)
	}
}
