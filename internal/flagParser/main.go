package flagParser

import (
	"flag"
	"os"
	"path/filepath"
)

type FlagArgs struct {
	ForceUpdate bool
	InstallDir  string
	NoSubdirs   bool
	PickRandom  bool
	Category    string
	ShowVersion bool
	JsonPath    string
}

func NewParser() FlagArgs {
	var flags FlagArgs

	flag.BoolVar(&flags.ForceUpdate, "force", false, "Forcefully update all wallpapers.")

	flag.StringVar(
		&flags.InstallDir,
		"directory",
		filepath.Join(os.Getenv("HOME"), ".local/share/wallpapers"),
		"Specify a custom installation directory.",
	)

	flag.BoolVar(
		&flags.NoSubdirs,
		"no-subdirs",
		false,
		"Prevent creating subdirectories for categories.",
	)

	flag.BoolVar(
		&flags.PickRandom,
		"pick-random",
		false,
		"Pick a random URL from wallpaper_urls.json and copy it to the system clipboard.",
	)
	flag.BoolVar(&flags.ShowVersion, "version", false, "Display version.")

	flag.StringVar(
		&flags.JsonPath,
		"input",
		"wallpaper_urls.json",
		"Path to wallpaper URLs JSON file",
	)

	flag.StringVar(
		&flags.Category,
		"category",
		"",
		"Select a category to pick a random wallpaper from or install wallpapers from that category only. \nAvailable categories: Abstract, Aesthetic, Animals, Architecture, Artistic, Bikes, Bing, Cars, Cityscapes, Cyberpunk, Dark, DragonBall, Fantasy, Flowers, Games, Macro, Minimalism, Naruto, Nature, Nord, Ocean, Ships, Space, Spiderman",
	)
	flag.Parse()

	return flags
}
