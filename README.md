# UWAL - Universal Wallpaper Downloader

[![License](https://img.shields.io/badge/license-MIT-blue.svg)](https://gitlab.com/sidmoreoss/uwal/-/blob/main/LICENSE)

**UWAL** (Universal Wallpaper Downloader) is a powerful Go-based command-line tool for downloading and managing wallpapers from various sources. It supports customizable installation directories, categories, and more, making it a versatile tool for wallpaper enthusiasts.

---

## Special thanks

[Bing wallpapers](https://github.com/ddddavid-he/bing-wallpaper-fetcher/blob/main/source_list.csv)

[Unsplash](https://unsplash.com/)

[WallpapersCraft](https://wallpaperscraft.com/)

[Wallhaven](https://wallhaven.cc/)

[Reddit r/wallpaper](https://www.reddit.com/r/wallpaper/)

[nordic-wallpapers](https://github.com/linuxdotexe/nordic-wallpapers)

[Nord-wallpapers](https://github.com/TheExacc/Nord-Wallpapers)

## Installation

### Using `go install`

To install UWAL, use `go install`:

```bash
go install gitlab.com/sidmoreoss/uwal@latest
```

### Using Nix Flakes

Nix flakes provide a reproducible and declarative way to build and install UWAL. Below is a step-by-step guide to installing UWAL using Nix flakes.

#### Step 1: Enable Flakes

If you haven't already enabled flakes in Nix, add the following line to your Nix configuration file:

- For **system-wide configuration**: Add to `/etc/nix/nix.conf`.
- For **user-specific configuration**: Add to `~/.config/nix/nix.conf`.

```ini
experimental-features = nix-command flakes
```

Restart the Nix daemon (if applicable) or restart your terminal session.

#### Step 2: Install UWAL

You can install UWAL directly using the following command:

```bash
nix profile install gitlab.com/sidmoreoss/uwal
```

This will download and install the latest version of UWAL.

#### Step 3: Build from Source (Optional)

If you prefer to build UWAL from source, follow these steps:

1. Clone the repository:

   ```bash
   git clone https://gitlab.com/sidmoreoss/uwal.git
   cd uwal
   ```

2. Build the project using the flake:

   ```bash
   nix build
   ```

   The built binary will be available in the `result/bin` directory.

3. Run UWAL directly from the build output:

   ```bash
   ./result/bin/uwal
   ```

#### Step 4: Enter Development Environment (Optional)

If you want to contribute to UWAL or work on the code, you can enter the development environment provided by the flake:

```bash
nix develop
```

This will provide you with all the necessary dependencies, including Go.

---

## Usage

UWAL provides a variety of command-line flags to customize its behavior. Below is a list of available flags and their descriptions.

### Available Flags

| Flag           | Description                                                                                                                                 |
| -------------- | ------------------------------------------------------------------------------------------------------------------------------------------- |
| `-force`       | Forcefully update all wallpapers.                                                                                                           |
| `-directory`   | Specify a custom installation directory. Default: `~/.local/share/wallpapers`.                                                              |
| `-no-subdirs`  | Prevent creating subdirectories for categories.                                                                                             |
| `-pick-random` | Pick a random URL from `wallpaper_urls.json` and copy it to the clipboard. Can be paired with `-category` to pick from a specific category. |
| `-version`     | Display the version of UWAL.                                                                                                                |
| `-input`       | Path to the `wallpaper_urls.json` file. Default: `../../wallpaper_urls.json`.                                                               |
| `-category`    | Select a category to filter wallpapers. See below for available categories.                                                                 |

### Categories

The `-category` flag allows you to filter wallpapers by specific categories. Available categories include:

- Abstract
- Aesthetic
- Animals
- Architecture
- Artistic
- Bikes
- Bing
- Cars
- Cityscapes
- Cyberpunk
- Dark
- DragonBall
- Fantasy
- Flowers
- Games
- Macro
- Minimalism
- Naruto
- Nature
- Ocean
- Ships
- Space
- Spiderman

---

## Examples

### Download Wallpapers to Default Directory

```bash
uwal
```

This will download wallpapers to the default directory (`~/.local/share/wallpapers`).

### Force Update All Wallpapers

```bash
uwal -force
```

Forcefully re-download all wallpapers, even if they already exist.

### Specify Installation Directory

```bash
uwal -directory=/path/to/custom/directory
```

Download wallpapers to a custom directory.

### Pick a Random Wallpaper URL

```bash
uwal -pick-random
```

Pick a random wallpaper URL from `wallpaper_urls.json` and copy it to the clipboard.

### Pick a Random Wallpaper from a Specific Category

```bash
uwal -pick-random -category=Nature
```

Pick a random wallpaper URL from the "Nature" category and copy it to the clipboard.

### Filter by Category

```bash
uwal -category=Nature
```

Download wallpapers only from the "Nature" category.

### Prevent Subdirectories

```bash
uwal -no-subdirs
```

Download all wallpapers directly to the installation directory without creating category subdirectories.

### Display Version

```bash
uwal -version
```

Display the current version of UWAL.

---

## Configuration File

UWAL supports configuration via a JSON file (`wallpaper_urls.json`). This file contains the URLs of wallpapers organized by category. You can specify a custom path to this file using the `-input` flag.

Example `wallpaper_urls.json` structure:

```json
{
  "Nature": [
    "https://example.com/nature1.jpg",
    "https://example.com/nature2.jpg"
  ],
  "Abstract": [
    "https://example.com/abstract1.jpg",
    "https://example.com/abstract2.jpg"
  ]
}
```

---

## Contributing

Contributions are welcome! If you'd like to contribute to UWAL, please follow these steps:

1. Fork the repository.
2. Create a new branch for your feature or bugfix.
3. Commit your changes.
4. Push your branch and open a merge request.

Please ensure your code adheres to the project's coding standards and includes appropriate tests.

---

## License

UWAL is licensed under the MIT License. See the [LICENSE](LICENSE) file for more details.

---

## Support

For questions, issues, or feature requests, please open an issue on the [GitLab repository](https://gitlab.com/sidmoreoss/uwal/-/issues).

---

**Disclaimer**: This project is under active development. APIs and features may change until a stable release is available.
