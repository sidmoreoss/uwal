package randomWallpaperPicker

import (
	"fmt"
	"math/rand"

	"github.com/atotto/clipboard"
)

type Wallpapers struct {
	Categories map[string][]string // Adjusted for your JSON structure
}

func Picker(wallpaperData map[string][]string, selectedCategory string) {
	if wallpaperData == nil {
		fmt.Println("No wallpaper data available.")
		return
	}

	var allWallpapers []string
	for category, urls := range wallpaperData {
		if selectedCategory == "" || category == selectedCategory {
			allWallpapers = append(allWallpapers, urls...)
		}
	}

	// Check if we have wallpapers
	if len(allWallpapers) == 0 {
		fmt.Println("No wallpapers available.")
		return
	}

	// Pick a random wallpaper
	selectedWallpaper := allWallpapers[rand.Intn(len(allWallpapers))]

	// Print the selected wallpaper
	fmt.Printf("Selected Wallpaper: %s\n", selectedWallpaper)

	// Copy to clipboard
	err := clipboard.WriteAll(selectedWallpaper)
	if err != nil {
		fmt.Printf("Failed to copy to clipboard: %v\n", err)
		return
	}
	fmt.Println("Wallpaper URL copied to clipboard!")
}
