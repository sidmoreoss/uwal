{
  description = "Uwal wallpaper manager";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";

    systems.url = "github:nix-systems/default";

    flake-utils = {
      url = "github:numtide/flake-utils";
      inputs.systems.follows = "systems";
    };
  };

  outputs =
    { flake-utils, ... }@inputs:
    flake-utils.lib.eachDefaultSystem (
      system:
      let
        pkgs = inputs.nixpkgs.legacyPackages.${system};
      in
      {
        packages.default = pkgs.buildGoModule {
          pname = "uwal";
          version = "v4.1.1";

          src = ../../.;

          patchPhase = ''
            substituteInPlace internal/flagParser/main.go \
            --replace "\"wallpaper_urls.json\"" "\"$src/wallpaper_urls.json\"" \
          '';

          vendorHash = null;
        };

        devshells.default = pkgs.mkShell {
          nativeBuildInputs = with pkgs; [
            go
          ];
        };
      }
    );
}
