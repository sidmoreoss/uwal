package utils

import (
	"crypto/md5"
	"encoding/hex"
	"fmt"
)

// ColoRed output
const (
	Green  = "\033[1;32m"
	Red    = "\033[0;31m"
	Yellow = "\033[1;33m"
	Reset  = "\033[0m"
	Orange = "\033[38;5;214m"
)

// Utility functions
func ShowMessage(msg string, color string) {
	fmt.Printf("%s%s%s\n", color, msg, Reset)
}

func GetHash(url string) string {
	hash := md5.Sum([]byte(url))
	return hex.EncodeToString(hash[:])[:10]
}
