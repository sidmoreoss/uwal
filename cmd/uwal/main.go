package main

import (
	"encoding/json"
	"fmt"
	"os"
	"sort"

	"github.com/sidmoreoss/uwal/internal/downloader"
	"github.com/sidmoreoss/uwal/internal/flagParser"
	"github.com/sidmoreoss/uwal/internal/randomWallpaperPicker"
	"github.com/sidmoreoss/uwal/internal/utils"
)

const version = "v4.1.1"

func main() {
	flags := flagParser.NewParser()

	if flags.ShowVersion {
		fmt.Printf("Version: %s\n", version)
		return
	}

	// Read JSON data
	file, err := os.Open(flags.JsonPath)
	if err != nil {
		utils.ShowMessage(
			fmt.Sprintf("%s not found. Please ensure it exists.", flags.JsonPath),
			utils.Red,
		)
		return
	}
	defer file.Close()

	var wallpaperData map[string][]string
	err = json.NewDecoder(file).Decode(&wallpaperData)
	if err != nil {
		utils.ShowMessage(fmt.Sprintf("Error parsing JSON: %v", err), utils.Red)
		return
	}

	// Collect supported categories
	var supportedCategories []string
	for category := range wallpaperData {
		supportedCategories = append(supportedCategories, category)
	}
	sort.Strings(supportedCategories)

	if flags.PickRandom {
		randomWallpaperPicker.Picker(wallpaperData, flags.Category)
		return
	}

	utils.ShowMessage(fmt.Sprintf("Installing wallpapers to: %s", flags.InstallDir), utils.Yellow)

	if flags.ForceUpdate {
		utils.ShowMessage(
			"Force update is enabled. Existing wallpapers will be overwritten.",
			utils.Orange,
		)
	}

	// Download wallpapers
	downloader.DownloadWallpapers(
		wallpaperData,
		flags.InstallDir,
		flags.ForceUpdate,
		flags.NoSubdirs,
		flags.Category,
	)
}
